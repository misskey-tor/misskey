/**
 * v6don
 */
import { highlight, ununesc } from './highlighter';

export type TextElementV6don = {
	type: 'v6don'
	content: string
	html: string
};

export default function(text: string) {
	const escapeText = ununesc(text);
	const v6don = highlight(escapeText);
	if (v6don == escapeText) return null;
	return {
		type: 'v6don',
		content: text,
		html: v6don
	} as TextElementV6don;
}
